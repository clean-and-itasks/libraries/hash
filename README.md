# hash

This library provides several hashing functions (SHA1, MD5).

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
